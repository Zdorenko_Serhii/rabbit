package xyz.daos.rabbit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import xyz.daos.rabbit.entity.Answer;
import xyz.daos.rabbit.entity.Question;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class Receiver {
    private List<Question> messages;

    @Value("${messages.number}")
    private int messagesNumber;

    @Value("${spring.rabbitmq.exchange}")
    private String exchange;

    @Value("${spring.rabbitmq.answerKey}")
    private String answerKey;

    @Autowired
    private RabbitTemplate template;

    public Receiver() {
        messages = new ArrayList<>();
    }


    @RabbitListener(queues = "${rabbitmq.queue.question}")
    public void receiveMessage(Question message) {
        log.info("receive question " + message);
        messages.add(message);
        if (messages.size() == messagesNumber) generateAnswer();
    }


    public void generateAnswer() {
        if (messages.size() == messagesNumber) {
            messages.forEach(m -> m.setAnswer(m.getFirst() + m.getSecond()));
            messages.forEach(m -> {
                if (m.getAnswer() % 7 == 0) {
                    m.setAnswer(11111); // make mistake
                }
                Answer answer = new Answer(m.getCorrelationId(), m.getAnswer());
                template.convertAndSend(exchange, answerKey, answer);
                log.info("send answer " + answer);
            });
            messages.clear();
        }
    }
}

