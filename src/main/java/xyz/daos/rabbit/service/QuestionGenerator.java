package xyz.daos.rabbit.service;

import org.springframework.stereotype.Component;
import xyz.daos.rabbit.entity.Question;

import java.util.Random;
import java.util.UUID;

@Component
public class QuestionGenerator {
    private static final Random random = new Random();

    private QuestionGenerator() {}

    public static Question generateMessage() {
        Question question = new Question();
        question.setCorrelationId(UUID.randomUUID());
        question.setFirst(random.nextInt(1000));
        question.setSecond(random.nextInt(1000));
        return question;
    }
}
