package xyz.daos.rabbit.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import xyz.daos.rabbit.entity.Answer;
import xyz.daos.rabbit.entity.InvalidMessage;
import xyz.daos.rabbit.entity.Question;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component
@Slf4j
public class Sender {
    private Map<UUID, Question> messages;

    public Sender() {
        messages = new HashMap<>();
    }

    @Autowired
    private RabbitTemplate template;

    @Autowired
    QuestionGenerator generator;

    @Value("${spring.rabbitmq.exchange}")
    private String exchange;

    @Value("${spring.rabbitmq.questionKey}")
    private String questionKey;

    @Value("${spring.rabbitmq.invalidKey}")
    private String invalidKey;

    public void sendMessage() {
        Question question = generator.generateMessage();
        messages.put(question.getCorrelationId(), question);
        template.convertAndSend(exchange, questionKey, question);
        log.info("send question " + question);
    }

    @RabbitListener(queues = "${rabbitmq.queue.answer}")
    public void receiveAnswer(Answer answer) {
        if (messages.isEmpty()) return;
        log.info("receive answer " + answer);
        Question question = messages.get(answer.getCorrelationId());
        if (!validateAnswer(question, answer)) {
            template.convertAndSend(exchange, invalidKey, new InvalidMessage(question.toString(), answer.toString()));
        }
        messages.remove(question.getCorrelationId());
        question.setAnswer(answer.getAnswer());
        log.info("message with answer " + question);
    }

    private boolean validateAnswer(Question message, Answer answer) {
        return answer.getAnswer() == message.getFirst() + message.getSecond();
    }
}
