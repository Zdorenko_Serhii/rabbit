package xyz.daos.rabbit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import xyz.daos.rabbit.service.Receiver;
import xyz.daos.rabbit.service.Sender;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class RabbitApplication {
    @Autowired
    Sender sender;

    @Autowired
    Receiver receiver;

    @Value("${messages.number}")
    private int messagesNumber;

    @PostConstruct
    private void start() {
        for (int i = 0; i < messagesNumber; i++) {
            sender.sendMessage();
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(RabbitApplication.class, args);
    }

}
