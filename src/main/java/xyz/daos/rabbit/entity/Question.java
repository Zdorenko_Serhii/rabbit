package xyz.daos.rabbit.entity;

import lombok.Data;

import java.util.UUID;

@Data
public class Question {
    private UUID correlationId;
    private int first;
    private int second;
    private int answer;
}
