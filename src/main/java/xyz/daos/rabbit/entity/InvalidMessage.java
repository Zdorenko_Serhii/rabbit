package xyz.daos.rabbit.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class InvalidMessage {
    @NonNull
    private String question;
    @NonNull
    private String answer;
}
